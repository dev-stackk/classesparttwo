package com.algebra.classesparttwo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Student student = new Student("Pero", 3);
        student.addGrade(3);
        student.addGrade(4);

        double averageGrade = student.calculateAverageGrade();

        System.out.println("Average grade of student: " + student.getName() + " is: " + averageGrade);

        Student student2 = new Student("Ivica", 5);
        student2.addGrade(5);
        student2.addGrade(4);
        student2.addGrade(3);
        student2.addGrade(5);

        student2.setYear(9);
        student2.setYear(4);

        System.out.println("All students: " + Student.getAllStudents());
        System.out.println("Lowest grade: " + student2.getLowestGrade());
        System.out.println("Highest grade: " + student2.getHighestGrade());
    }
}
