package com.algebra.classesparttwo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Student {

    private String name;
    private int year;
    private List<Integer> grades;

    private static List<Student> allStudents = new ArrayList<>();

    public Student(String name, int year) {
        this.name = name;
        this.year = year;
        this.grades = new ArrayList<>();
        allStudents.add(this);
    }

    public void addGrade(int grade) {
        if (grade > 0 && grade <= 5) {
            System.out.println("Adding grade: " + grade);
            grades.add(grade);
            return;
        }

        System.out.println("Invalid grade, please enter valid grade");
    }

    public double calculateAverageGrade() {
        int total = 0;
        for (int i = 0; i < grades.size(); i++) {
            total = total + grades.get(i);
        }

        return total / (double) grades.size();
    }

    public int getLowestGrade() {
        Collections.sort(this.grades);
        return grades.get(0);
    }

    public int getHighestGrade() {
        Collections.sort(this.grades);
        return grades.get(grades.size() - 1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() > 2) {
            this.name = name;
            return;
        }
        System.out.println("Invalid name (too short)");
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (year <= 5 && year > 0) {
            this.year = year;
            System.out.println("Year updated");
            return;
        }

        System.out.println("Invalid year");
    }

    public static List<Student> getAllStudents() {
        return allStudents;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", grades=" + grades +
                '}';
    }
}
